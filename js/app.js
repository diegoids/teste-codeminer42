function LogController($scope, $window) {
    $scope.itens = [
        {tempo: '1h', tipo: 'Corrida', data: '10/10/2014'},
        {tempo: '3h', tipo: 'Natação', data: '09/10/2014'},
        {tempo: '5h', tipo: 'Bicicleta', data: '01/10/2014'}
    ];

    $scope.tipos = [
        {name: 'Corrida'},
        {name: 'Bicicleta'},
        {name: 'Natação'}
    ];

    function calculateHours(){
        var total = 0;
        for (var i = $scope.itens.length - 1; i >= 0; i--) {
            total = parseInt(total) + parseInt($scope.itens[i].tempo.replace('h', ''));
        };
        return total;
    }

    $scope.horas = calculateHours();

    $scope.logItem = function () {
        //Validate form
        var valid = true,
            regexData = new RegExp(/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/),
            regexNum = new RegExp(/^\d*[0-9]{1,2}$/);

        if($('#tempo').val() == '' || !regexNum.test($('#tempo').val()) ){
            $('#tempo').addClass('error');
            valid = false;
        } else {
            $('#tempo').removeClass('error');
            valid = true;

            if($('#tipo').val().indexOf('?') == 0){
                $('#tipo').addClass('error');
                valid = false;
            } else {
                $('#tipo').removeClass('error');
                valid = true;

                if($('#data').val() == '' || !regexData.test($('#data').val()) ){
                    $('#data').addClass('error');
                    valid = false;
                } else {
                    $('#data').removeClass('error');
                    valid = true;
                }

            }

        }


        //apply values
        if(valid){
            $scope.itens.push({tempo: $scope.item.tempo + 'h',
                               tipo: $scope.item.tipo,
                               data: $scope.item.data});
            $scope.item.tempo = $scope.item.tipo = $scope.item.data = '';
            $scope.horas = calculateHours();
        }
    };

    $scope.removeItem = function (logItem) {
        var confirm = $window.confirm('Tem certeza que deseja remover este item?');
        if(confirm){
            var idx = $scope.itens.indexOf(logItem);
            $scope.itens.splice(idx, 1);
            $scope.horas = calculateHours();
        }
    };
}
$(function() {
    $('#data').mask('99/99/9999');
});